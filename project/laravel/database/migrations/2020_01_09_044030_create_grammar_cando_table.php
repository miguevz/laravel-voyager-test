<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrammarCandoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grammar_cando', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('grammar_id')->unsigned();
            $table->foreign('grammar_id')->references('id')->on('grammars')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('cando_id')->unsigned();
            $table->foreign('cando_id')->references('id')->on('cando_objectives')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grammar_cando');
    }
}
