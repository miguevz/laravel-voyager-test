<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatedGrammarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('related_grammar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('grammar_id_1')->unsigned();
            $table->foreign('grammar_id_1')->references('id')->on('grammars')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('grammar_id_2')->unsigned();
            $table->foreign('grammar_id_2')->references('id')->on('grammars')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('related_grammar');
    }
}
